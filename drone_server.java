import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

class MyClass
{

    private static final int BCAST_PORT = 8779;
    private static DatagramSocket mSocket;

    private static String telemString = "telem";
    private static String commandString = "command";
    private static String statusString = "status";


    public static void main(String[] args)
    {
   
        try 
        {
            if (mSocket == null) 
            {
                mSocket = new DatagramSocket(null);
                mSocket.setReuseAddress(true);
                mSocket.setBroadcast(true);
                mSocket.bind(new InetSocketAddress(BCAST_PORT));

                while (true) 
                {
                    // will hold the data
                    byte[] message = new byte[1024];

                    // listen for data
                    DatagramPacket packet = new DatagramPacket(message,message.length);
                    ///Log.i("UDP client: ", "about to wait to receive");
                    mSocket.receive(packet);

                    // display the data
                    String text = new String(message, 0, packet.getLength());
                    //Log.i(TAG,"UDP primary Received data: " + text);
                    System.out.println("Received string " + text);

                    if(text.equals("telem?"))
                    {
                        System.out.println("Received telem request");

                        message = telemString.getBytes();

                        packet = new DatagramPacket(message, message.length, packet.getAddress(), packet.getPort());

                        mSocket.send(packet);

                        System.out.println("Sent " + telemString + " to device " + packet.getAddress() + " on port " + packet.getPort());
                    }
                    else if(text.contains("telem"))
                    {
                        System.out.println("Received telem srting"); 

                        telemString = text;
                    }
                    else if(text.equals("command?"))
                    {
                        System.out.println("Received command request");
                        
                        message = commandString.getBytes();
                        
                        packet = new DatagramPacket(message, message.length, packet.getAddress(), packet.getPort());
                        
                        mSocket.send(packet);

                        System.out.println("Sent " + commandString + " to device " + packet.getAddress() + " on port " + packet.getPort());
                    
                        commandString = "command"; // reset string to avoid same commands being sent multiple times
                    }
                    else if (text.contains("command"))
                    {
                        System.out.println("Received command srting"); 
                        
                        commandString = text;
                    }
                    if(text.equals("status?"))
                    {
                        System.out.println("Received status request");

                        message = statusString.getBytes();

                        packet = new DatagramPacket(message, message.length, packet.getAddress(), packet.getPort());

                        mSocket.send(packet);

                        System.out.println("Sent " + statusString + " to device " + packet.getAddress() + " on port " + packet.getPort());
                    }
                    else if(text.contains("status"))
                    {
                        System.out.println("Received status srting"); 

                        statusString = text;
                    }
                    
                    System.out.println("--"); 
                }
            }
        } 
        catch (IOException e) 
        {
            
        }
    }
}
